let express = require("express"), cors = require('cors');
let app = express();
app.use(express.json());
// cors es una libreria de npm que ayuda a proteger los servidores desde donde pueden enviar peticiones a nuestra aplicacion 
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

let ciudades = [ "Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile", "Mexico DF", "Nueva York" ];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

let misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
  console.log(req.body);
  misDestinos.push(req.body.nuevo);
  res.json(misDestinos);
});

// traducciones
app.get("/api/translation", (req, res, next) => res.json([
  // lang str del lenguaje 
  // key representa el texto a traducir 
  // value el valor a traducir 
  {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));
