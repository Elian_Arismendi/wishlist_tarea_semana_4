import { DestinoViaje } from './destino-viaje.model'; 
import { BehaviorSubject, Subject } from 'rxjs'; 
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { appConfig, AppState, APP_CONFIG, db } from '../app.module'; 
import { Store } from '@ngrx/store'; 
import { forwardRef, Inject, Injectable } from '@angular/core';
// http 
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from "@angular/common/http";


@Injectable()
export class DestinosApiClient {
    destinos = []; 
    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: appConfig,
        private http: HttpClient
    ) {
        this.store
        .select(state => state.destinos)
        .subscribe((data) => {
            this.destinos = data.items;
        });
        this.store
        .subscribe((data) => {
            console.log('all store');
            console.log(data);
        });
    }

    add(d: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDB = db; 
                // se carga en la base de datos
                myDB.destinos.add(d); 
                
                // destinos hace referencia a la tabla que almacena los json 
                // toArray() es la forma de hacer una consulta, devuelve una promesa
                myDB.destinos.toArray().then(destinos => console.log(destinos)); 
            }
        });
    }

    getById(id: String): DestinoViaje {
        return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    elegir(d: DestinoViaje) {
        // aqui incovariamos al servidor
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }
}
































// DEPRECATED
// @Injectable()
// export class DestinosApiClient {
//     // Subject es un observable que funciona como el EventEmitter, lo instanciamos de forma tal de que en toda la aplicacion esta pendiente del comportamiento de las instancias destinoViaje
//     // Este en particular esta pendiente del destino elegido como favorito 
//     // public current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null); // lo que es pasado al constructor es el valor por def. 
//     destinos = []; 
//     constructor(private store: Store<AppState>){
        
//     }
//     add(destino: DestinoViaje): void {
//         this.store.dispatch(new NuevoDestinoAction(destino)); 
//     }
//     escogerComoFavorito(destino: DestinoViaje): void {
//        this.store.dispatch(new ElegidoFavoritoAction(destino)); 
//     }
//     getById(id: String): DestinoViaje {
//         return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
//     }
// }