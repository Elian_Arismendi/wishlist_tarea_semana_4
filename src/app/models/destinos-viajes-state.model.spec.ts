import {
    reducerDestinosViajes,
    DestinosViajesState,
    initalizeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction
  } from './destinos-viajes-state.model';
import { ComponentFixture, TestBed } from '@angular/core/testing';
  import { DestinoViaje } from './destino-viaje.model';
  
  describe('reducerDestinosViajes', () => {
    // test 1 
    it('should reduce init data', () => {
    // setup: armar los objetos necesarios para testear
      const prevState: DestinosViajesState = initalizeDestinosViajesState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    //action: actuar/realizar acciones sobre el codigo 
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    // assertions: verificaciones, validar salidas esperadas  
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].nombre).toEqual('destino 1');
    // tearDown (no usado) etapa donde se borran datos de testing que se hayan creado con el fin de probar el codigo 
    });
    
    // test 2 
    it('should reduce new item added', () => {
      const prevState: DestinosViajesState = initalizeDestinosViajesState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
      const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].nombre).toEqual('barcelona');
    });
  });