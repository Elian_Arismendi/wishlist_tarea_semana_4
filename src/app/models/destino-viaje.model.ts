import {v4 as uuid} from 'uuid';


export class DestinoViaje { 
    // forma normal
    // constructor(nombre:string, imgUrl:string) {
    //     this.nombre = nombre; 
    //     this.imgUrl = imgUrl; 
    // }
   
    
    // uuid() es un identificador unico universal 
    id = uuid();
    public servicios: string[]; 
    private selected: boolean; 
     // shortcut
    constructor(public nombre: string, public imgUrl:string, public votes = 0) {
        this.servicios = ['pileta', 'desayuno']; 
    }
    get Selected(): boolean {
        return this.selected; 
        
    }
    setSelected(selected: boolean) { 
        this.selected = selected; 
    }
    isSelected() {
        return this.selected;
    }
    voteUp(): any {
        this.votes++;
    }
    voteDown(): any {
        this.votes--;
    }
}