import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax'; 
import { DestinoViaje } from '../../models/destino-viaje.model';
import { appConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>; 
  public fg: FormGroup; // Form group representa al grupo de elementos del formulario ya construido (line 15)
  public minLong:number = 6; 
  public searchResults: string[]; 
  // forwardRef() se utiliza para frenar la referencia circular (app.module importa este componente y  este componente importa algo de app.module), ya que genera una excepcion  
  constructor(formsBuilder: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: appConfig) { //Form builder nos permite construir el formulario 
    this.onItemAdded = new EventEmitter(); 
    this.fg = formsBuilder.group({  //Aca los construye

      // Estos son formControls que creamos para vincular a los tags html correspondientes 
      nombre: ['', Validators.compose([Validators.required, this.nombreValidators, this.nombreValidatorParametrizable(this.minLong)])], //Validators son validaciones por defecto de angular ---- .compose  permite colocar un array de validadores
      url: [''], 

    }); 
  }

  ngOnInit(): void {
    let elementoNombre = <HTMLInputElement>document.getElementById('nombre'); // seleccionar input 
    let valorElemento: string; 
    //fromEvent genera un observable de eventos de entrada
    fromEvent(elementoNombre, 'input') // cada vez que se toca una tecla 

      //instruccion que sirve para declarar una secuencia de acciones a seguir en serie
      .pipe(
        //definir que es un evento de teclado y obtener el valor de la cadena resultante 
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        // filtra el texto (omite el resto de la ejecucion) si es menor a dos caracteres
        filter(text => text.length > 2), 
        // se esperan 200 milisegundos a que ingresen nuevos valores de entrada, si no ocurre continua 
        debounceTime(200),
        // aguarda por si cambian o se actualizan valores en la cadena de texto 
        distinctUntilChanged(), 
        // nuevo 
        // Consumir la api de express con ajax
        switchMap((text:string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);

    }
  // deprecated 
  //       // consumir un json esperando obtener un resultado
  //       switchMap(()=> ajax('/assets/datos.json'))
  //     ).subscribe(ajaxResponse => {
  //       // asignarle los resultados
  //       this.searchResults = ajaxResponse.response; 
  //     });  
  

  guardar(nombre: string, url: string): boolean {
    const destino: DestinoViaje = new DestinoViaje(nombre, url);  
    this.onItemAdded.emit(destino); 
    return false; //para evitar acciones por defecto al submitear
  }

  // Validaciones para el campo nombre del formulario, que retorna un objeto con una key formato string que almacena un boolean
  nombreValidators(control: FormControl): { [key:string]: boolean } { 
    // control representa el campo del formulario que se esta validando y control.value es su valor  
    const long = control.value
                      .toString() //Fuerza a que el valor sea string
                      .trim() // Si tiene espacios en blanco los suprime 
                      .length; //Devuelve la cantidad de caracteres luego del formateo 
  
    if( long > 0 && long < 5) {
      return { invalidNombre: true }; 
    }
    return null; 
  }
  // ValidatorFn es una libreria de angular 
  nombreValidatorParametrizable(minLong: number):ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      const long = control.value.toString().trim().length; 
      if(long > 0 && long < minLong) {
        return { minLongNombre: true }; 
      } 
      return null; 
    }
  }
}

