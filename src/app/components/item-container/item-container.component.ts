import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model'; 
import { Store } from '@ngrx/store'; 
import { AppState } from '../../app.module'; 
@Component({
  selector: 'app-item-container',
  templateUrl: './item-container.component.html',
  styleUrls: ['./item-container.component.css'],
  // Los servicios que no sean utilizados por muuchos componentes pueden declararse directamente en los componentes que lo usan 
  providers: [DestinosApiClient] 
})
export class ItemContainerComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>; 
  all;
  public updateDestFavoritos: string[]; 
  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {  // Se inyecta/inicializa el servicio 
    this.onItemAdded = new EventEmitter();
    this.updateDestFavoritos = []; 
    this.store.select(state => state.destinos.favorito) 
      .subscribe(data => {
        if(data !== null) {
          // se agrega al array de favoritos el destino
          this.updateDestFavoritos.push(`Se ha elegido a: ${data.nombre}`); 
        }
      }); 
    this.store.select(state => state.destinos.items).subscribe(items => this.all = items); 
    
    
}

  ngOnInit(): void {
  }

  agregado(destino: DestinoViaje): void {
    this.destinosApiClient.add(destino); 
    // this.onItemAdded.emit(destino);
  }

  destinoElegido(destino: DestinoViaje): void { 
    this.destinosApiClient.elegir(destino);
  }

}
