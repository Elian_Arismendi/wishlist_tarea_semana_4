import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VuelosDetalleComponentComponent } from './vuelos-detalle-component/vuelos-detalle-component.component';
import { VuelosMainComponentComponent } from './vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './vuelos-mas-info-component/vuelos-mas-info-component.component';

export const routes: Routes = [
    {
        path:'',
        redirectTo: 'main', 
        pathMatch: 'full'
    },
    {
        path:'main',
        component: VuelosMainComponentComponent, 
    },
    {
        path:'mas-info',
        component: VuelosMasInfoComponentComponent, 
    },
    {
        path:':id',
        component: VuelosDetalleComponentComponent, 
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
  