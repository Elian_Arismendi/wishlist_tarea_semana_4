import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model'; 
import { DestinoApiClientViejoService } from 'src/app/services/destino-api-client-viejo.service';

// =======================
// EJEMPLO SERVICE ANTIGUO
// ======================= 

// En una version anterior, en este componente se utilizaba DestinoApiClientViejoService 
// Pero en versiones siguientes se decidio actualizar el provider a destinosApiClient 
// Como el proceso de actualizacion en toda la app todavia se esta realizando, el servicio inyectado sigue siendo destinosApiClientViejo. 
// **Por ende para ordenar al componente que utilize el nuevo provider en vez del anterior se raliza lo siguiente 

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  // Los servicios que no sean utilizados por muchos componentes pueden declararse directamente en los componentes que lo usan 
  providers: [
  DestinosApiClient, 
 
    // deprecated
  //  // **Se declara primero el nuevo provider
  //   DestinosApiClient, 
  //   // se utiliza la siguiente configuracion para reemplazar el proveedor (para poder reemplazar proveedores de esta forma deben compartir una implementacion equivalente) 
  //   {
  //     provide: DestinoApiClientViejoService, 
  //     useExisting: DestinosApiClient
  //   } 
  ] 
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  // style = {
  //   sources: {
  //     world: {
  //       type: 'geojson',
  //       data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
  //     }
  //   },
  //   version: 8,
  //   layers: [{
  //     'id': 'countries',
  //     'type': 'fill',
  //     'source': 'world',
  //     'layout': {},
  //     'paint': {
  //       'fill-color': '#6F788A'
  //     }
  //   }]
  // };

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
