import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store'; 
import { AppState } from '../../app.module'; 
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model'; 
import { animate, state, style, transition, trigger } from '@angular/animations';
@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'], 
  // animaciones
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})

export class ItemComponent implements OnInit {
  @Input() destino: DestinoViaje; // Con input se declara que esta variable es suceptible a ser pasada como parametro en el tag item-container 
  @Input('index') position: number; // No es buena practica, pero pueden renombrarse los input y en algunas ocaciones es util 
  @HostBinding('attr.class') cssClass = 'col-md-4'; //Cuando se renderiza el elemento se enlaza con el atributo class de el wrapper que crea angular por defecto
  @Output() clicked: EventEmitter<DestinoViaje> // con output se declara que esta propiedad puede enviar datos al componente padre
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter(); // inicializando EventEmitter se usa para emitir eventos personalizados 
  }

  ngOnInit(): void {
  }

  ir() {
    this.clicked.emit(this.destino); 
    return false // para que no se ejecute ningun evento por def. 
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino)); 
    return false; 
    
  }
  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino)); 
    return false; 
  }



}
