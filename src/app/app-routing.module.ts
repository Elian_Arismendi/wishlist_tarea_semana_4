import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemContainerComponent } from './components/item-container/item-container.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { LoginComponent } from './components/login/login.component'; 
import { ProtectedComponent }  from './components/protected/protected.component'; 
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { routes as vuelosRoutes } from './components/vuelos/vuelos-routing.module'; 
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ItemContainerComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  // en este path se hace referencia tanto a vuelos como a sus propias rutas (con "children") y al guard (con "canActivate")
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: vuelosRoutes
  }
]; 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
