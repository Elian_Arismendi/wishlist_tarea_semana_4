import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-wish-list';
  constructor(public translate: TranslateService) {
    console.log('get translation');
    // para probar pide la traduccion de ingles
    translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    //  setea el default lang en spanish
    translate.setDefaultLang('es');
  }
  
  time = new Observable(observer => {
    setInterval(() => {
      observer.next(new Date().toString()); 
    }, 1000);
  }); 

}
