import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
// import { RouterModule, Routes }from '@angular/router'; //importaciones para rutas;  
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; //importaciones para formularios 
// http 
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from "@angular/common/http";

// import dexie 
import  Dexie  from 'dexie'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
// import { NgxMapboxGLModule } from 'ngx-mapbox-gl'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludadorComponent } from './components/saludador/saludador.component';
import { ItemComponent } from './components/item/item.component';
import { ItemContainerComponent } from './components/item-container/item-container.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesState, reducerDestinosViajes, initalizeDestinosViajesState, DestinosViajesEffects, InitMyDataAction } from './models/destinos-viajes-state.model';
import { ActionReducerMap, Store, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component'; 
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// declarando las rutas SE REALIZA EN EL APP ROUTING MODULE 
// const routes: Routes = [
//   {
//     path: '', 
//     redirectTo: 'home', //al path que redirecciona
//     pathMatch: 'full' //que matchee exactamente cuando esta vacio, osea que se cumpla al 100% la condicion declarada en el path
//   }, 
//   {
//     path: 'home', 
//     component: ItemContainerComponent
//   }
//   ,
//   {
//     path: 'destino/:id', 
//     component: DestinoDetalleComponent
//   }
// ]; 

// redux init
  export interface AppState { 
    // definiendo estado global de la app 
    destinos: DestinosViajesState; 
  }
  // reducers globales
  const reducers: ActionReducerMap<AppState> = {
    destinos: reducerDestinosViajes
  } 
  let reducersInitialState = { 
    destinos: initalizeDestinosViajesState()
  }
// redux fin init 

// Api config 
export interface appConfig {
  apiEndpoint: String; 
}

export const APP_CONFIG_VALUE: appConfig = {
  apiEndpoint: 'http://localhost:3000'
}

export const  APP_CONFIG = new InjectionToken<appConfig>('app.config'); 

// fin 

// app init (carga la config de la api de express)
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

// fin app init


// INIT DEXIE DB 
// Crea servicio inyectable 
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}
@Injectable({
  providedIn: 'root'
}) 

export class myDatabase extends Dexie {
  // se define una tabla 
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>; 
  constructor() {
    super('MyDatabase');
    // se define la version, y lo que guardara
    this.version(1).stores({ 
      // se guarda un objeto que tiene un id, nombre e imagen
      destinos: '++id, nombre, imgUrl'
    }); 
    // de esta manera (versionando), se puede marcar como fue evolucionando la base de datos 
    this.version(2).stores({
      destinos: '++id, nombre, imgUrl', 
      translations: '++id, lang, key, value'
    }); 
  }
}

// se exporta una instancia de la bd 
export const db = new myDatabase(); 
// END 


// i18n ini  cargador de traducciones 
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    // cuando piden la traduccion 
    const promise = db.translations
                      // si lang en la bd es igual al pasado por parametro 
                      .where('lang')
                      .equals(lang)
                      // devuelve una promesa de resultados 
                      .toArray()
                      .then(results => {
                                        // si no existe en la bd 
                                        if (results.length === 0) {
                                          // hace la peticion ajax
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              // se agrega a la bd 
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        // si se encuentra en la bd retorna los resultados
                                        return results;
                                      })
                                      // cuando ya tenemos el array de traducciones 
                                      .then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      })
                                      // aca se mapea como quiere la libreria ngxtranslate 
                                      // que espera un objeto con clave y valor
                                      .then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });

  // transformamos esta promesa a un observable porque es lo que espera angular
   return from(promise)
  // la promesa retorna un array de arrays, por ende en la siguiente linea transformamos eso en un array de str que son los idiomas 
   .pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    ItemComponent,
    ItemContainerComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective,
    // CommonModule
 
  ],
  imports: [
    // Deben importarse los modulos para ser usados
    BrowserModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
    // NgxMapboxGLModule,
    // http 
    HttpClientModule, 
    // RouterModule.forRoot(routes), //registrando las rutas en el modulo para poder utilizarlas
    FormsModule,
    ReactiveFormsModule, 
    // Redux 
    NgRxStoreModule,
    NgRxStoreModule.forRoot(reducers, 
      {
        initialState: reducersInitialState, 

        runtimeChecks: {
          strictStateImmutability: false, 
          strictActionImmutability: false
        }
      },
    ),       
    BrowserAnimationsModule,
    EffectsModule.forRoot([DestinosViajesEffects]), 
    StoreDevtoolsModule.instrument(), ReservasModule, 
  ],
  providers: [  /*se registra el servicio creado*/, 
    AuthService, 
    UsuarioLogueadoGuard, 
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},  
    AppLoadService,
    // app initializer es un injection token provisto por angular que es una funcion que retorna un objeto que realiza ciertas tareas cuando se inicialize la aplicacion (una especie de middleware)
    // cuando se inicia la app se busca todas las injecciones de dependecias que se hicieron con app initializer
    // con use factory hacemos la inyeccion de  dependencia 
    // y deps dice que antes de cargar la inyeccion de factory se debe cargar las dependencias de esta propiedad, por eso se declara antes tambien
    // multi: true permite tener varios app initializer declarados 
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }, 
    myDatabase, 
  ],
  bootstrap: [AppComponent] //Inicializador
})
export class AppModule { }
