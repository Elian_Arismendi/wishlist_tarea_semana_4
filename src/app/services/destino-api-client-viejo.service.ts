import { Injectable } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Injectable({
  providedIn: 'root'
})
export class DestinoApiClientViejoService {

  constructor() { }
  
// =======================
// EJEMPLO SERVICE ANTIGUO
// ======================= 

// En una version anterior, en este componente se utilizaba este servicio 
  getById(id:String): DestinoViaje {
    console.log('Hola mundo desde el provider viejo');
    return null;
  }
}
