import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

constructor() { }
// validar el login
login(user: string, password: string): boolean {
  // al loguear guarda el usuario en el localstorage 
  if (user === 'user' && password === 'password') {
    localStorage.setItem('username', user);
    return true;
  }
  return false;
}

// controla el logout 
logout(): any {
  localStorage.removeItem('username');
}

// retorna el usuario logueado s
getUser(): any {
  return localStorage.getItem('username');
}

isLoggedIn(): boolean {
  return this.getUser() !== null;
}
}
