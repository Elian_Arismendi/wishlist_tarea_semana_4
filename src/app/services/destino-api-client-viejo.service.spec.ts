import { TestBed } from '@angular/core/testing';

import { DestinoApiClientViejoService } from './destino-api-client-viejo.service';

describe('DestinoApiClientViejoService', () => {
  let service: DestinoApiClientViejoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DestinoApiClientViejoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
